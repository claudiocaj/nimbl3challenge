package com.example.nimbl3.takethesurvey.manager;

public class AccessTokenManager {
    private static AccessTokenManager instance = null;
    private String accessToken;

    public static AccessTokenManager getInstance() {
        if (instance == null) {
            instance = new AccessTokenManager();
        }
        return instance;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
