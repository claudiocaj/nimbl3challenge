package com.example.nimbl3.takethesurvey.view.surveys.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.nimbl3.takethesurvey.R;
import com.example.nimbl3.takethesurvey.manager.SurveysManager;
import com.example.nimbl3.takethesurvey.model.ToolBarType;
import com.example.nimbl3.takethesurvey.view.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChosenSurveyActivity extends BaseActivity {
    @BindView(R.id.surveyName)
    TextView surveyName;

    @BindView(R.id.backgroundImage)
    ImageView surveyImage;

    SurveysManager manager = SurveysManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        //change the tool bar icon to back icon
        changeToolBarType(ToolBarType.BACKTYPE);

        //get the index chosen
        Intent intent = getIntent();
        int indexChosen = intent.getIntExtra("surveyIndex", 0);
        loadNameImage(indexChosen);
    }

    /**
     * load the name and the image of the survey selected
     *
     * @param index the index of the survey chosen
     */
    private void loadNameImage(int index) {
        surveyName.setText(manager.getSurveys().get(index).getTitle());
        Glide.with(this)
                .load(manager.getSurveys().get(index).getImageUrl())
                .into(surveyImage);
    }

    @Override
    public void onRefreshOrBackPressed() {
        finish();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_chosen_survey;
    }
}
