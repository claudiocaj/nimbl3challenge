package com.example.nimbl3.takethesurvey.view;

public interface BaseView {

    public void showToast(String msg);

    public void showError();

    public void showProgressDialog(String msg);

    public void dismissProgress();

    public void onRefreshOrBackPressed();

}
