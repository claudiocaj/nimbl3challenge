package com.example.nimbl3.takethesurvey.presenter.surveys;

import com.example.nimbl3.takethesurvey.manager.AccessTokenManager;
import com.example.nimbl3.takethesurvey.manager.ServiceGenerator;
import com.example.nimbl3.takethesurvey.manager.SurveysManager;
import com.example.nimbl3.takethesurvey.model.Survey;
import com.example.nimbl3.takethesurvey.presenter.BasePresenter;
import com.example.nimbl3.takethesurvey.service.SurveysClient;
import com.example.nimbl3.takethesurvey.view.surveys.interfaces.SurveysView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SurveysPresenter extends BasePresenter<SurveysView> implements SurveyPresenterInterface {


    public SurveysPresenter(SurveysView view) {
        super(view);
    }

    /**
     * Make the request to get the surveys information
     */
    public void getSurveys() {
        getView().showDialogLoadingSurveys();
        SurveysClient client = ServiceGenerator.createService(SurveysClient.class);
        Call<List<Survey>> call = client.getSurveys(AccessTokenManager.getInstance().getAccessToken());
        call.enqueue(new Callback<List<Survey>>() {
            @Override
            public void onResponse(Call<List<Survey>> call, Response<List<Survey>> response) {
                getView().dismissProgress();
                SurveysManager.getInstance().setSurveys(response.body());
                getView().showTheSurveys();
            }

            @Override
            public void onFailure(Call<List<Survey>> call, Throwable t) {
                getView().dismissProgress();
                getView().showError();
            }
        });
    }
}
