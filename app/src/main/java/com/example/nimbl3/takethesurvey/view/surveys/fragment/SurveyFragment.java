package com.example.nimbl3.takethesurvey.view.surveys.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.example.nimbl3.takethesurvey.R;
import com.example.nimbl3.takethesurvey.manager.SurveysManager;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SurveyFragment extends Fragment {
    private static final String ARG_PARAM1 = "index";
    private View rootView;
    SurveysManager manager;

    //index of the survey list
    private int index;

    //name of the survey
    @BindView(R.id.surveyName)
    TextView surveyName;

    //background image of the survey
    @BindView(R.id.backgroundImage)
    ImageView imageBackground;

    public SurveyFragment() {
        // Required empty public constructor
    }

    public static SurveyFragment newInstance(int index) {
        SurveyFragment fragment = new SurveyFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, index);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_PARAM1);
        }
        manager = SurveysManager.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_survey, container, false);
        ButterKnife.bind(this, rootView);
        setSurveyNameAndImage();
        return rootView;
    }


    /**
     * set the survey name and description
     */
    private void setSurveyNameAndImage() {
        surveyName.setText(manager.getSurveys().get(index).getTitle());

        Glide.with(this)
                .load(manager.getSurveys().get(index).getImageUrl())
                .into(imageBackground);
    }

}
