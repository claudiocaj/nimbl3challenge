package com.example.nimbl3.takethesurvey;


import android.app.Application;

import com.example.nimbl3.takethesurvey.manager.AccessTokenManager;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AccessTokenManager.getInstance().setAccessToken(BuildConfig.accessToken);
    }

}
