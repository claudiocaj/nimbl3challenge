package com.example.nimbl3.takethesurvey.view.surveys.interfaces;

import com.example.nimbl3.takethesurvey.view.BaseView;

public interface SurveysView extends BaseView {

    public void showDialogLoadingSurveys();

    public void showTheSurveys();
}
