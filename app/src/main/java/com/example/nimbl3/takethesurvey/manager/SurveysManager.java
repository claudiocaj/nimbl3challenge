package com.example.nimbl3.takethesurvey.manager;

import com.example.nimbl3.takethesurvey.model.Survey;

import java.util.List;

public class SurveysManager {
    List<Survey> surveys;
    public static SurveysManager instance = null;

    public static SurveysManager getInstance() {
        if (instance == null) {
            instance = new SurveysManager();
        }
        return instance;

    }

    public List<Survey> getSurveys() {
        return surveys;
    }

    public void setSurveys(List<Survey> surveys) {
        this.surveys = surveys;
    }


}
