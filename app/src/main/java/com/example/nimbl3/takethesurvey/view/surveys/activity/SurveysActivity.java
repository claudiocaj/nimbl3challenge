package com.example.nimbl3.takethesurvey.view.surveys.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.nimbl3.takethesurvey.R;
import com.example.nimbl3.takethesurvey.adapter.SurveysAdapter;
import com.example.nimbl3.takethesurvey.presenter.surveys.SurveyPresenterInterface;
import com.example.nimbl3.takethesurvey.presenter.surveys.SurveysPresenter;
import com.example.nimbl3.takethesurvey.view.BaseActivity;
import com.example.nimbl3.takethesurvey.view.surveys.interfaces.SurveysView;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import me.kaelaela.verticalviewpager.VerticalViewPager;
import me.relex.circleindicator.CircleIndicator;

public class SurveysActivity extends BaseActivity implements SurveysView {
    private int indexSelected;
    //string of loading surveys
    @BindString(R.string.loading_surveys)
    String loadingSurveys;

    //view pages of the surveys
    @BindView(R.id.surveyviewPager)
    VerticalViewPager surveysViewPage;

    //bullets indicators
    @BindView(R.id.indicator)
    CircleIndicator indicator;

    //presenter of this activity
    public SurveyPresenterInterface presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initControls();
    }

    /**
     * Init the variables and start to find the surveys
     */
    private void initControls() {
        presenter = new SurveysPresenter(this);
        presenter.getSurveys();
    }

    @Override
    public void showDialogLoadingSurveys() {
        showProgressDialog(loadingSurveys);
    }

    @Override
    public void showTheSurveys() {
        SurveysAdapter adapter = new SurveysAdapter(getSupportFragmentManager());
        surveysViewPage.setAdapter(adapter);

        //get the index about the survey selected page
        surveysViewPage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                indexSelected = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        configureIndicatorsBullets();
    }

    @Override
    public void onRefreshOrBackPressed() {
        presenter.getSurveys();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_surveys;
    }

    /**
     * go to the selected survey
     */
    @OnClick(R.id.takeSurveyBtn)
    void goToSurvey() {
        Intent i = new Intent(this, ChosenSurveyActivity.class);
        i.putExtra("surveyIndex", indexSelected);
        startActivity(i);
    }

    /**
     * Function to configure the indicator bullets according to the surveys quantity
     */
    private void configureIndicatorsBullets() {
        indicator.configureIndicator(50, 50, 20);
        indicator.setOrientation(LinearLayout.VERTICAL);
        indicator.setViewPager(surveysViewPage);
    }
}
