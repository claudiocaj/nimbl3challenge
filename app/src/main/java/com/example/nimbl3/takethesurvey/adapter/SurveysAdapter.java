package com.example.nimbl3.takethesurvey.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.nimbl3.takethesurvey.manager.SurveysManager;
import com.example.nimbl3.takethesurvey.view.surveys.fragment.SurveyFragment;

public class SurveysAdapter extends FragmentPagerAdapter {

    public SurveysAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return SurveysManager.getInstance().getSurveys().size();
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        return SurveyFragment.newInstance(position);
    }

}

