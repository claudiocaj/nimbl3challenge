package com.example.nimbl3.takethesurvey.model;

import com.google.gson.annotations.SerializedName;

public class Survey {
    @SerializedName("title")
    String title;
    @SerializedName("description")
    String description;
    @SerializedName("cover_image_url")
    String imageUrl;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
