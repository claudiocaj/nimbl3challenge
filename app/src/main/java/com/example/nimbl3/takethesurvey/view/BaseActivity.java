package com.example.nimbl3.takethesurvey.view;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageButton;
import android.widget.Toast;
;

import com.example.nimbl3.takethesurvey.R;
import com.example.nimbl3.takethesurvey.model.ToolBarType;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public abstract class BaseActivity extends AppCompatActivity implements BaseView {
    protected ProgressDialog mProgressDialog;

    @BindView(R.id.toolBar)
    protected Toolbar toolBar;

    @BindView(R.id.refreshOrBack)
    protected ImageButton refreshOrBack;

    protected abstract int getLayoutResourceId();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(this.getLayoutResourceId());
        ButterKnife.bind(this);
        setSupportActionBar(toolBar);
    }

    /**
     * Show the toast
     *
     * @param msg the message that will be shown on the toast
     */
    @Override
    public void showToast(String msg) {
        Toast.makeText(BaseActivity.this, msg, Toast.LENGTH_SHORT).show();
    }


    /**
     * Show error on Toast
     */
    @Override
    public void showError() {
        showToast(getResources().getString(R.string.error));
    }

    /**
     * Show a progress dialog to the user
     *
     * @param msg the message on the progress dialog
     */
    @Override
    public void showProgressDialog(String msg) {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            dismissProgress();
        mProgressDialog = ProgressDialog.show(this, getResources().getString(R.string.app_name), msg);
    }

    /**
     * dismiss the progress dialog that is running.
     */
    @Override
    public void dismissProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @OnClick(R.id.refreshOrBack)
    public void refresh() {
        onRefreshOrBackPressed();
    }

    /**
     * change the type of the tool bar.Depending of the type, the upper left icon could be a refresh or back button
     * @param type the type
     */
    protected void changeToolBarType(ToolBarType type) {
        switch (type) {
            case BACKTYPE:
                refreshOrBack.setBackground(getResources().getDrawable(R.drawable.back));
                break;
            case REFRESHTYPE:
                refreshOrBack.setBackground(getResources().getDrawable(R.drawable.refresh));
                break;
        }
    }

}
