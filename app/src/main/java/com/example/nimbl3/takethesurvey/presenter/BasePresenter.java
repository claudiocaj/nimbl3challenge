package com.example.nimbl3.takethesurvey.presenter;

public class BasePresenter<V> {
    protected final V view;

    public BasePresenter(V view) {
        this.view = view;
    }

    public V getView() {
        return view;
    }
}
