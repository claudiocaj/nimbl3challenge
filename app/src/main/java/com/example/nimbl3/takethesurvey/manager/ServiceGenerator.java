package com.example.nimbl3.takethesurvey.manager;

import android.os.Build;

import com.example.nimbl3.takethesurvey.BuildConfig;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    private static final String BASE_URL = BuildConfig.baseUrl;

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    /**
     * Create a service with the base url of gist api
     *
     * @param serviceClass
     * @param <S>
     * @return
     */
    public static <S> S createService(
            Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }

    /**
     * Create a service with a different baseurl
     *
     * @param serviceClass
     * @param baseUrl
     * @param <S>
     * @return
     */
    public static <S> S createService(
            Class<S> serviceClass, String baseUrl) {
        Retrofit.Builder tempbuilder =
                new Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create());
        Retrofit tempRetrofit = tempbuilder.build();
        return tempRetrofit.create(serviceClass);
    }
}
