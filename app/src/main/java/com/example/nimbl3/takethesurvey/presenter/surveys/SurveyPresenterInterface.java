package com.example.nimbl3.takethesurvey.presenter.surveys;

public interface SurveyPresenterInterface {

    public void getSurveys();
}
