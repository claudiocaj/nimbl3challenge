package com.example.nimbl3.takethesurvey.service;

import com.example.nimbl3.takethesurvey.model.Survey;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SurveysClient {

    @GET("/surveys.json")
    Call<List<Survey>> getSurveys(@Header("Authorization") String authorizationToken);

    @GET("/surveys.json")
    Call<ResponseBody> getSurvey(@Header("Authorization") String authorizationToken);
}
